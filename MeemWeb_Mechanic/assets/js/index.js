//File to control user login page
var username, password, login;
function redirector(){
	
	username = document.getElementById("username").value;
	password = document.getElementById("password").value;
	
	//check for login remember values
	if(document.getElementById('remember').checked){
		login = "remember";		
	} else {
		login = "no";
	}
	
	//check for user or mechanic
	if(document.getElementById('user-login').checked){
		role = document.getElementById('user-login').value;
	} else if(document.getElementById('mech-login').checked){
		role = document.getElementById('mech-login').value;
	}
	
	//check for emtpy fields
	
		
		//login remember
		if(login === "remember" ){
			setCookie("username", username, 20);
			setCookie("password", password, 20);
			setCookie("role", role, 20);
		} else{
			//when login not remember
			setCookie("username", username, 0.1);
			setCookie("password", password, 0.1);
			setCookie("role", role, 0.1);
		}
        
        //rediection based on user or mechanic
		if(role === "user"){
			window.open("home.html", "_self");
		}else if(role === "mech"){
			window.open("mechanic_home.html", "_self");
		}
			
		
}

function validate(){
		//take in the values
		username = document.getElementById("username").value;
		password = document.getElementById("password").value;
		if(password == "" | username==""){
		alert("Please enter username and password");
	}else{
		
		//jquery code to check password
		$.ajax({
                type: "POST",
                url: "CheckPassword.php",
				dataType: 'text',
                success: function(json_data){
                    var jsonObj = $.parseJSON(json_data);

                   if(password === jsonObj['password']){
						alert("You are verified");
						redirector();
					}
					else{
						alert(jsonObj.password);
					}
                }
            });
			
	}
/*			//ajax code to verify password
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				confirm("ready state = "+xmlhttp.readyState+ "status" + xmlhttp.status);
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					alert(xmlhttp.responseText);
					var jsonObj = JSON.parse(xmlhttp.responseText);
					if(password === jsonObj.password){
						alert("You are verified");
					}
					else{
						alert(jsonObj.password);
					}
				}
			};
		xmlhttp.open("GET", "CheckPassword.php?a=0", true);
		xmlhttp.send();
*/
}

//set the cookie in the browser
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}


//get the cookie stored in the browser
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}


//checks for the cookie from home page and redirects to login in case of no cookie
function checkCookieHome() {
		var roll=getCookie("role");
		if (roll=="" || roll=== "mech"){
			window.open("index.html", "_self");
		}
}

//checks for the cookie from mechanic home page and redirects to login in case of no cookie
function checkCookieHomeMechanic() {
		var roll=getCookie("role");
		if (roll=="" || roll==="user"){
			window.open("index.html", "_self");
		}
}

//checks for the cookie from login page and redirects to home page when cookie is found
function checkCookieIndex() {
    var role=getCookie("role");
    if(role === "user"){
			window.open("home.html", "_self");
	}else if(role === "mech"){
			window.open("mechanic_home.html", "_self");
		}
}

//deletes all the stored cookies
function deleteCookie(){
	document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
	document.cookie = "password=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
	document.cookie = "role=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
}

//used to log out
function logout(){
	var yes=confirm("Are you sure to logout?");
	if(yes===true){
		deleteCookie();
		window.open("index.html", "_self");
	}
}